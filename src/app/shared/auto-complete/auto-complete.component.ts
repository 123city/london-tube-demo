import { Component, OnInit, forwardRef, Output, EventEmitter, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.scss'],
  providers: [{
    multi: true,
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AutoCompleteComponent)
  }]
})
export class AutoCompleteComponent implements ControlValueAccessor, OnInit {
  public query: string;
  private queryChanged: Subject<string> = new Subject<string>();
  private deboundeTime = 500;
  private subscription: Subscription;

  @Input() public options: any[];
  @Output() public search = new EventEmitter<string>();

  private propagateChange: any;
  private onTouched: any;

  constructor() { }

  ngOnInit() {
    this.subscription = this.queryChanged
      .pipe(debounceTime(this.deboundeTime))
      .subscribe(() => this.loadOptions());
  }

  registerOnChange(fn: any): void { this.propagateChange = fn; }
  registerOnTouched(fn: any): void { this.onTouched = fn; }

  private onChange(event) {
    // update the form
    this.propagateChange(event);
  }

  public didType($event): void {
    console.log('did type', $event);
    this.queryChanged.next($event);
  }

  writeValue(value: any) {
    console.log('FORM => COMPONENT', value);
  }

  loadOptions(): void {
    this.search.emit(this.query);
    console.log('load options', this.query);
  }

  select(id: number, text: string) {
    this.query = text;
    this.onChange(id);
  }

}
