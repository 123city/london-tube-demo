export interface StatusUpdateResultInterface {
    id: string;
    name: string;
    modeName: string;
    disruptions: any[];
    created: string;
    modified: string;
    lineStatuses: any[];
}
