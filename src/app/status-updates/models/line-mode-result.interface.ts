export interface LineModeResultInterface {
    '$type': string;
    'id': string;
    'name': string;
    'modeName': string;
    'disruptions': any[];
    'created': string;
    'modified': '2019-03-28T12:17:56.673Z';
    'lineStatuses': any[];
    'routeSections': any[];
    'serviceTypes': any[];
    'crowding': any;
}
