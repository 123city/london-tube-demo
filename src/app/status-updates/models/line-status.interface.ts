export interface LineStatusInterface {
    backgroundColor: string;
    foregroundColor: string;
    name: string;
    status: string;
}
