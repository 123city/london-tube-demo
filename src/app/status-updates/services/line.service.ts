import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LineModeResultInterface } from '../models/line-mode-result.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LineService {

  constructor(private http: HttpClient) { }

  public getLineMode(): Observable<LineModeResultInterface[]> {
    return this.http.get<LineModeResultInterface[]>('https://api.tfl.gov.uk/Line/Mode/tube');
  }

  public getLineIds() {
    return this.getLineMode().pipe(map(line => line.map(l => {
      return l.id;
    })));
  }
}
