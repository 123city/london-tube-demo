import { TestBed } from '@angular/core/testing';

import { StatusUpdatesService } from './status-updates.service';

describe('StatusUpdatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatusUpdatesService = TestBed.get(StatusUpdatesService);
    expect(service).toBeTruthy();
  });
});
