import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StatusUpdateResultInterface } from '../models/status-update-result.interface';

@Injectable({
  providedIn: 'root'
})
export class StatusUpdatesService {
  constructor(private http: HttpClient) { }

  public getStatusUpdates(commaseperatedLineIds: string): Observable<StatusUpdateResultInterface[]> {
    return this.http.get<StatusUpdateResultInterface[]>('https://api.tfl.gov.uk/Line/' +
      encodeURI(commaseperatedLineIds) + '/Status?detail=false');
  }
}
