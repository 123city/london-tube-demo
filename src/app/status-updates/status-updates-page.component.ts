import { Component, OnInit } from '@angular/core';
import { LineStatusInterface } from './models/line-status.interface';
import { LineService } from './services/line.service';
import { LineModeResultInterface } from './models/line-mode-result.interface';
import { StatusUpdatesService } from './services/status-updates.service';
import { StatusUpdateResultInterface } from './models/status-update-result.interface';

@Component({
  selector: 'app-status-updates-page',
  templateUrl: './status-updates-page.component.html',
  styleUrls: ['./status-updates-page.component.scss']
})
export class StatusUpdatesPageComponent implements OnInit {
  public lines: string[];

  public lineStatuses: StatusUpdateResultInterface[] = [];

  constructor(
    private lineService: LineService,
    private statusUpdatesService: StatusUpdatesService) { }

  ngOnInit() {
    this.lineService.getLineIds().subscribe(lines => {
      this.lines = lines;
      this.loadStatusUpdates(lines.join(','));
    });
  }

  private loadStatusUpdates(commaSeperatedLineIds): void {
    this.statusUpdatesService.getStatusUpdates(commaSeperatedLineIds).subscribe(result => {
      this.lineStatuses = result;
    });
  }

}
