import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusUpdatesPageComponent } from './status-updates-page.component';

describe('StatusUpdatesPageComponent', () => {
  let component: StatusUpdatesPageComponent;
  let fixture: ComponentFixture<StatusUpdatesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusUpdatesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusUpdatesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
