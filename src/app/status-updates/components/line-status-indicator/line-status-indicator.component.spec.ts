import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineStatusIndicatorComponent } from './line-status-indicator.component';

describe('LineStatusIndicatorComponent', () => {
  let component: LineStatusIndicatorComponent;
  let fixture: ComponentFixture<LineStatusIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineStatusIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineStatusIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
