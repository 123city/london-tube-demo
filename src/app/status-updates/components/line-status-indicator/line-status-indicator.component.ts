import { Component, OnInit, Input } from '@angular/core';
import { LineStatusInterface } from '../../models/line-status.interface';

@Component({
  selector: 'app-line-status-indicator',
  templateUrl: './line-status-indicator.component.html',
  styleUrls: ['./line-status-indicator.component.scss']
})
export class LineStatusIndicatorComponent implements OnInit {
  @Input() lineStatus: LineStatusInterface;
  opened = false;

  constructor() { }

  ngOnInit() {
  }

}
