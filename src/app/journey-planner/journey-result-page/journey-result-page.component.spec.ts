import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourneyResultPageComponent } from './journey-result-page.component';

describe('JourneyResultPageComponent', () => {
  let component: JourneyResultPageComponent;
  let fixture: ComponentFixture<JourneyResultPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourneyResultPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourneyResultPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
