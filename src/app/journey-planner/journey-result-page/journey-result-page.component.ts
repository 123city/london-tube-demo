import { Component, OnInit, Input } from '@angular/core';
import { JourneyResultsInterface } from '../interfaces/journey-results-interface';

@Component({
  selector: 'app-journey-result-page',
  templateUrl: './journey-result-page.component.html',
  styleUrls: ['./journey-result-page.component.scss']
})
export class JourneyResultPageComponent implements OnInit {
  @Input() result: JourneyResultsInterface;

  constructor() { }

  ngOnInit() {
  }

}
