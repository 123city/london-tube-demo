import { TestBed } from '@angular/core/testing';

import { StopPointSearchService } from './stop-point-search.service';

describe('StopPointSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StopPointSearchService = TestBed.get(StopPointSearchService);
    expect(service).toBeTruthy();
  });
});
