import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { JourneyResultsInterface } from '../interfaces/journey-results-interface';

@Injectable({
  providedIn: 'root'
})
export class JourneyResultsService {

  constructor(private http: HttpClient) { }

  public getJourneyResults(from: string, to: string, date: string, time: string, timeIs: string): Observable<JourneyResultsInterface[]> {
    return this.http.get<JourneyResultsInterface[]>('https://api.tfl.gov.uk/Journey/JourneyResults/' +
      encodeURI(from) +
      '/to/' + encodeURI(to) +
      '?date=' + encodeURI(date) +
      '&time=' + encodeURI(time) +
      '&timeIs=' + encodeURI(timeIs) +
      '&app_key=b01ae502830a2dcd458f5dfd32c87aff&app_id=d5e60b70');
  }
}
