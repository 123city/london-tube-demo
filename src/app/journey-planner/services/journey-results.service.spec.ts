import { TestBed } from '@angular/core/testing';

import { JourneyResultsService } from './journey-results.service';

describe('JourneyResultsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JourneyResultsService = TestBed.get(JourneyResultsService);
    expect(service).toBeTruthy();
  });
});
