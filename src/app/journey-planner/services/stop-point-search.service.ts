import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StopPointSearchService {

  constructor(private http: HttpClient) { }

  public getJourneyResults(query: string): Observable<any[]> {
    return this.http.get<any[]>('https://api.tfl.gov.uk/StopPoint/Search/' +
      encodeURI(query) +
      '?maxResults=10' +
      '&tflOperatedNationalRailStationsOnly=true' +
      '&app_key=b01ae502830a2dcd458f5dfd32c87aff&app_id=d5e60b70');
  }
}
