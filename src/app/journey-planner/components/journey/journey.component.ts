import { Component, OnInit, Input } from '@angular/core';
import { JourneyInterface } from '../../interfaces/journey-results-interface';

@Component({
  selector: 'app-journey',
  templateUrl: './journey.component.html',
  styleUrls: ['./journey.component.scss']
})
export class JourneyComponent implements OnInit {
  @Input() journey: JourneyInterface;

  constructor() { }

  ngOnInit() {
  }

}
