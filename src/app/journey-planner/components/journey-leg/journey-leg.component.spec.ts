import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourneyLegComponent } from './journey-leg.component';

describe('JourneyLegComponent', () => {
  let component: JourneyLegComponent;
  let fixture: ComponentFixture<JourneyLegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourneyLegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourneyLegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
