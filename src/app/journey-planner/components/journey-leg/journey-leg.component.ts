import { Component, OnInit, Input } from '@angular/core';
import { JourneyLegInterface } from '../../interfaces/journey-results-interface';

@Component({
  selector: 'app-journey-leg',
  templateUrl: './journey-leg.component.html',
  styleUrls: ['./journey-leg.component.scss']
})
export class JourneyLegComponent implements OnInit {
  @Input() leg: JourneyLegInterface;
  showAllStops = false;

  constructor() { }

  ngOnInit() {
  }

}
