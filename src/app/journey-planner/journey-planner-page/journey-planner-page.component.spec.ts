import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourneyPlannerPageComponent } from './journey-planner-page.component';

describe('JourneyPlannerPageComponent', () => {
  let component: JourneyPlannerPageComponent;
  let fixture: ComponentFixture<JourneyPlannerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourneyPlannerPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourneyPlannerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
