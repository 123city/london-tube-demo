import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JourneyResultsService } from '../services/journey-results.service';
import { JourneyResultsInterface } from '../interfaces/journey-results-interface';
import { StopPointSearchService } from '../services/stop-point-search.service';

@Component({
  selector: 'app-journey-planner-page',
  templateUrl: './journey-planner-page.component.html',
  styleUrls: ['./journey-planner-page.component.scss']
})
export class JourneyPlannerPageComponent implements OnInit {
  form: FormGroup;
  result: JourneyResultsInterface;

  fromOptions: any[];
  toOptions: any[];

  constructor(private readonly formBuilder: FormBuilder,
              private journeyResultsService: JourneyResultsService,
              private stopPointSearchService: StopPointSearchService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      to: ['1000013', Validators.required],
      from: ['1000266', Validators.required],
      timeIs: ['departing', Validators.required],
      date: ['20190412', Validators.required],
      time: ['1200', Validators.required]
    });
  }

  save(): void {
    if (!this.form.valid) {
      alert('something is invalid');
    } else {
      console.log(this.form.getRawValue());

      this.journeyResultsService.getJourneyResults(
        this.form.get('from').value,
        this.form.get('to').value,
        this.form.get('date').value,
        this.form.get('time').value,
        this.form.get('timeIs').value
      ).subscribe((data: any) => {
        this.result = data;
      });

    }
  }

  searchFrom($event: string) {
    this.stopPointSearchService.getJourneyResults($event).subscribe(data => {
      this.fromOptions = data;
    });
  }

  searchTo($event: string) {
    this.stopPointSearchService.getJourneyResults($event).subscribe(data => {
      this.toOptions = data;
    });
  }

}
