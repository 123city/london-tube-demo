export interface JourneyResultsInterface {
    $type: string;
    journeys: JourneyInterface[];
}

export interface JourneyInterface {
    $type: string;
    startDateTime: string;
    duration: number;
    arrivalDateTime: string;
    legs: JourneyLegInterface[];
    fare: any;
}

export interface JourneyLegInterface {
    $type: string;
    duration: number;
    instruction: JourneyInstructionInterface;
    obstacles: JourneyObstacleInterface[];
    departureTime: string;
    arrivalTime: string;
    departurePoint: JourneyPointInterface;
    arrivalPoint: JourneyPointInterface;
    path: JourneyPathInterface;
    routeOptions: any[];
    mode: JourneyModeInterface;
    disruptions: any[];
    plannedWorks: any[];
    isDisrupted: boolean;
    hasFixedLocations: boolean;
}

export interface JourneyObstacleInterface {
    $type: string;
    type: string;
    incline: string;
    stopId: number;
    position: string;
}

export interface JourneyInstructionInterface {
    $type: string;
    summary: string;
    detailed: string;
    steps: JourneyStepInterface[];
}

export interface JourneyStepInterface {
    $type: string;
}

export interface JourneyPointInterface {
    $type: string;
    platformName: string;
    icsCode: string;
    commonName: string;
    placeType: string;
    additionalProperties: any[];
    lat: number;
    lon: number;
}

export interface JourneyPathInterface {
    $type: string;
    lineString: string;
    stopPoints: any[];
    elevation: any[];
}

export interface JourneyModeInterface {
    $type: string;
    id: string;
    name: string;
    type: string;
    routeType: string;
    status: string;
}
