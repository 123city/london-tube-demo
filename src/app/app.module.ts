import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StatusUpdatesPageComponent } from './status-updates/status-updates-page.component';
import { LineStatusIndicatorComponent } from './status-updates/components/line-status-indicator/line-status-indicator.component';
import { HttpClientModule } from '@angular/common/http';
import { JourneyPlannerPageComponent } from './journey-planner/journey-planner-page/journey-planner-page.component';
import { JourneyResultPageComponent } from './journey-planner/journey-result-page/journey-result-page.component';
import { DashboardPageComponent } from './dashboard/dashboard-page/dashboard-page.component';
import { HeaderComponent } from './shared/header/header.component';
import { PageContainerComponent } from './shared/page-container/page-container.component';
import { PageTitleComponent } from './shared/page-title/page-title.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JourneyLegComponent } from './journey-planner/components/journey-leg/journey-leg.component';
import { AutoCompleteComponent } from './shared/auto-complete/auto-complete.component';
import { JourneyComponent } from './journey-planner/components/journey/journey.component';

@NgModule({
  declarations: [
    AppComponent,
    StatusUpdatesPageComponent,
    LineStatusIndicatorComponent,
    JourneyPlannerPageComponent,
    JourneyResultPageComponent,
    DashboardPageComponent,
    HeaderComponent,
    PageContainerComponent,
    PageTitleComponent,
    JourneyLegComponent,
    AutoCompleteComponent,
    JourneyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
