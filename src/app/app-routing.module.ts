import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatusUpdatesPageComponent } from './status-updates/status-updates-page.component';
import { JourneyPlannerPageComponent } from './journey-planner/journey-planner-page/journey-planner-page.component';
import { DashboardPageComponent } from './dashboard/dashboard-page/dashboard-page.component';

const routes: Routes = [
  {
      path: '',
      pathMatch: 'full',
      redirectTo: 'dashboard'
  },
  {
      path: 'dashboard',
      component: DashboardPageComponent
  },
  {
      path: 'journey-planner',
      component: JourneyPlannerPageComponent
  },
  {
      path: 'status-updates',
      component: StatusUpdatesPageComponent
  },
  {
      path: 'notFound',
      component: StatusUpdatesPageComponent
  },
  {
      path: 'oops',
      component: StatusUpdatesPageComponent
  },
  {
      path: '**',
      redirectTo: 'notFound'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
